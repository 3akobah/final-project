package com.front.goldfinger.modells;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {

    @NotNull(message = "Username is required")
    @Size(min = 2, message = "Min Username length is 2")
    private String username;

    @NotNull(message = "Password is required")
    @Size(min = 2, message = "Password is required")
    private String password;

    private String passwordConfirmation;

    public User() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }
}
