package com.front.goldfinger.services;

import java.util.Map;

public interface AuditMapDataReceiver {

    Map<String, Object>  getDataMap(Map<String, Object> data);
}
