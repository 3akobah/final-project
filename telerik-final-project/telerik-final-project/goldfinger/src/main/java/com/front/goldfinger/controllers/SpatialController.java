package com.front.goldfinger.controllers;

import com.front.goldfinger.modells.Request;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@Api(value="Goldfinger Spatial System", description="Operations pertaining to redirecting to the Spatial API of the Goldfinger Management System")
@Controller
@RequestMapping("/spatial")
public class SpatialController {

    @ApiOperation(value = "Get the home page URL of Spatial API.", response = String.class)
    @GetMapping("/showmap")
    public String getMap() {
        return "index";
    }




}
