package com.front.goldfinger.services;

import com.front.goldfinger.exceptions.AuditApiConnectionFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

//@Component("myAuthenticationSuccessHandler")
@Service
public class LoginSuccessHandler implements AuthenticationSuccessHandler {


    private AuditMapDataReceiver auditMapDataReceiver;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    public LoginSuccessHandler(AuditMapDataReceiver auditMapDataReceiver) {
        this.auditMapDataReceiver = auditMapDataReceiver;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {


        HttpSession session = request.getSession();
        if (session != null) {

            Map<String, Object> map  = new HashMap<>();
            map.put("event", "Login");

            try {
                auditMapDataReceiver.getDataMap(map);
            } catch (AuditApiConnectionFailedException ae) {
                handleFail(request, response, authentication);
                session.invalidate();
            }
        }

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication)
            throws IOException {

        String targetUrl = "http://localhost:8090/home";

        if (response.isCommitted()) {

            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected void handleFail(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication)
            throws IOException {

        String targetUrl = "http://localhost:8090/fail";

        if (response.isCommitted()) {

            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

//    protected String determineTargetUrl(Authentication authentication) {
//        boolean isUser = false;
//        boolean isAdmin = false;
//        Collection<? extends GrantedAuthority> authorities
//                = authentication.getAuthorities();
//        for (GrantedAuthority grantedAuthority : authorities) {
//            if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
//                isAdmin = true;
//                break;
//            } else if (grantedAuthority.getAuthority().equals("ROLE_USER")) {
//                isUser = true;
//                break;
//            }
//        }
//
//        if (isUser) {
//            return "/home";
//        } else if (isAdmin) {
//            return "/auditing/search";
//        } else {
//            throw new IllegalStateException();
//        }
//    }

    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}
