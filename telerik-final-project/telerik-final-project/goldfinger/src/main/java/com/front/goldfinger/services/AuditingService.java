package com.front.goldfinger.services;

import com.front.goldfinger.modells.LogEntry;
import com.front.goldfinger.modells.SearchDto;

import java.util.List;
import java.util.Map;

public interface AuditingService  {

    String indexLogEntry( Map<String, Object> data);

    List<LogEntry> findAll ();

}
