package com.front.goldfinger.config;


import com.front.goldfinger.exceptions.AuditApiConnectionFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.RestClientException;
import org.springframework.web.servlet.ModelAndView;


@ControllerAdvice
public class GlobalExceptionHandler implements ErrorController {

    @Autowired
    public GlobalExceptionHandler() {
    }

    @Override
    public String getErrorPath() {
        return "/error/";
    }



//    @ExceptionHandler(value = UserNotExistException.class)
//    public ModelAndView handleUserNotExistException(Exception ex) {
//        ModelAndView modelAndView = new ModelAndView("custom_error_pages/user_not_exist");
//
//        modelAndView.addObject("msg", ex.getMessage());
//
//        return modelAndView;
//    }

    @ExceptionHandler(value = AuditApiConnectionFailedException.class)
    public ModelAndView handleUserNotExistException(Exception ex) {
        ModelAndView modelAndView = new ModelAndView("custom_error_pages/something_went_wrong");



        return modelAndView;
    }




}
