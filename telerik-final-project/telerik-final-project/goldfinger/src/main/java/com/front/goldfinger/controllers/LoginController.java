package com.front.goldfinger.controllers;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@Api(value="Goldfinger Login System", description="Operations pertaining to user login in the Goldfinger Management System")
public class LoginController {

    @ApiOperation(value = "Get the login page URL.", response = String.class)
    @GetMapping("/login")
    public String showLogin() {
        return "login";
    }

    @ApiOperation(value = "Get the access-denied page URL.", response = String.class)
    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }

    @ApiOperation(value = "Get the login-error page URL.", response = String.class)
    @GetMapping("/fail")
    public String showFail() {
        return "/custom_error_pages/something_went_wrong";
    }
}
