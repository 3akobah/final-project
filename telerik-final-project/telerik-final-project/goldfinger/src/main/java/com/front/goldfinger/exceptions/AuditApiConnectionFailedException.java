package com.front.goldfinger.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class AuditApiConnectionFailedException extends RuntimeException {

    public AuditApiConnectionFailedException() {
        super("No connection to audit api ");
    }

}