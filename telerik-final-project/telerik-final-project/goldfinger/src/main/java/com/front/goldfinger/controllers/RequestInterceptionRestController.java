package com.front.goldfinger.controllers;


import com.front.goldfinger.modells.Request;
import com.front.goldfinger.services.RequestConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class RequestInterceptionRestController {

    private RequestConverter requestConverter;

    @Autowired
    public RequestInterceptionRestController(RequestConverter requestConverter) {
        this.requestConverter = requestConverter;
    }

    @PostMapping("/api/spatial/getRequestPointData")
    public void getPointData (@RequestBody Request point) throws  Exception{
        requestConverter.getIncomingRequest(point);
    }
}
