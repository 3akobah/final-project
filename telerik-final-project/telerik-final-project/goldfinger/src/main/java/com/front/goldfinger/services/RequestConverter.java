package com.front.goldfinger.services;

import com.front.goldfinger.modells.Request;

public interface RequestConverter {

    Request getIncomingRequest(Request request);

}
