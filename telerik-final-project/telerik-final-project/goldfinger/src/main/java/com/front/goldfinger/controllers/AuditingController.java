package com.front.goldfinger.controllers;

import com.front.goldfinger.modells.LogEntry;
import com.front.goldfinger.modells.SearchDto;
import com.front.goldfinger.services.AuditingService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Api(value="Goldfinger Auditing System", description="Operations pertaining to auditing in the Goldfinger Management System")
@Controller
@RequestMapping("/auditing")
public class AuditingController {

    private AuditingService auditingService;

    @Autowired
    public AuditingController(AuditingService auditingService) {
        this.auditingService = auditingService;
    }

    @ApiOperation(value = "Get the search page URL.", response = String.class)
    @GetMapping("/search")
    public String getSearchPage(Model model) {
        model.addAttribute("searchDto", new SearchDto());

        return "auditing/home_search";
    }



}
