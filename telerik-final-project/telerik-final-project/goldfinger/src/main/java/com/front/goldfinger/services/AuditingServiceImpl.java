package com.front.goldfinger.services;

import com.front.goldfinger.exceptions.AuditApiConnectionFailedException;
import com.front.goldfinger.modells.LogEntry;
import com.front.goldfinger.modells.SearchDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Service
public class AuditingServiceImpl implements AuditingService, AuditMapDataReceiver {

    private RestTemplate restTemplate;

    private String API_URL = "http://localhost:8081/api/audit";

    @Autowired
    public AuditingServiceImpl (RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();

    }


    @Override
    public List<LogEntry> findAll () {
        ParameterizedTypeReference<List<LogEntry>> ptrl = new ParameterizedTypeReference<List<LogEntry>>() {} ;
        ResponseEntity<List<LogEntry>> reResult = restTemplate.exchange(API_URL +"/all", HttpMethod.GET,null, ptrl);

        List<LogEntry> resultList = reResult.getBody();

        return resultList;
    }

    @Override
    public Map<String, Object> getDataMap(Map<String, Object> data) {
        indexLogEntry(data);
        return data;
    }

    public String indexLogEntry( Map<String, Object> data) {
        String result = "";

        String username;
        if (data.containsKey("username")) {
            username = data.get("username").toString();
            data.remove("username");
        } else {

            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            username = ((UserDetails) principal).getUsername();
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
        String ip = request.getRemoteAddr();

        LogEntry logEntry = new LogEntry();
        logEntry.setUsername(username);
        logEntry.setIp(ip);

        logEntry.setDetails(data);


        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        HttpEntity<LogEntry> entity = new HttpEntity<>(logEntry, headers);


        try {
            result = restTemplate.exchange(API_URL + "/addNew", HttpMethod.POST, entity, String.class).getBody();
        } catch (RestClientException rce) {
            throw new AuditApiConnectionFailedException();
        }

        return result;


    }
}
