package com.front.goldfinger.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.front.goldfinger.modells.Request;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class RequestConverterMap implements RequestConverter {

    private AuditMapDataReceiver auditMapDataReceiver;

    private ObjectMapper objectMapper;

    public RequestConverterMap(AuditMapDataReceiver auditMapDataReceiver, ObjectMapper objectMapper) {
        this.auditMapDataReceiver = auditMapDataReceiver;
        this.objectMapper = objectMapper;
    }

    @Override
    public Request getIncomingRequest(Request request) {
        transfer(request);
        return request;
    }

    private Map<String, Object> convertRequestToMap(Request request) {
        return objectMapper.convertValue(request, Map.class);
    }

    private void sendData(Map<String, Object> data) {
        auditMapDataReceiver.getDataMap(data);
    }

    private void transfer(Request request) {


        Map<String, Object> data;
        data = convertRequestToMap(request);


        sendData(data);

    }


}
