package com.front.goldfinger.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.stereotype.Service;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class LogoutUserSuccessHandler implements LogoutSuccessHandler {

    private AuditMapDataReceiver auditMapDataReceiver;

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Autowired
    public LogoutUserSuccessHandler(AuditMapDataReceiver auditMapDataReceiver) {
        this.auditMapDataReceiver = auditMapDataReceiver;
    }

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response,
                                Authentication authentication) throws IOException, ServletException {

        HttpSession session = request.getSession();
        if (session != null){
            Map<String, Object> map  = new HashMap<>();

            String username = authentication.getName();

            map.put("username", username);
            map.put("event", "Logout");


            auditMapDataReceiver.getDataMap(map);
            session.removeAttribute("user");
        }

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);

    }

    protected void handle(HttpServletRequest request,
                          HttpServletResponse response, Authentication authentication)
            throws IOException {

        String targetUrl = "http://localhost:8090/login?logout";

        if (response.isCommitted()) {

            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }


    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }
}
