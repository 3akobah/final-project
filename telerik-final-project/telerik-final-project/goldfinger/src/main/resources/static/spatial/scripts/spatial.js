$(document).ready(function () {
    let currentShape;

    let layer = null;
    let marker = null;

    let latX = null;
    let latY = null;

    let soilShape = 'soil';
    let earthquakeShape = 'earthquake';

    $(document).on('change', '.shape-selector', function () {

        currentShape = $(this).val();

        layerGroup.clearLayers();
        info.update();
        if (marker !== null) {
            map.removeLayer(marker);
        }
    });




    //MAP ZOOM/PAN CONTROL START
    let southWest = L.latLng(-89.98155760646617, -180);
    let northEast = L.latLng(89.99346179538875, 180);
    let bounds = L.latLngBounds(southWest, northEast);
    //MAP ZOOM/PAN CONTROL END

    //CURSOR CONTROL START
    let map = L.map('mapid', {
        minZoom: 3, zoomControl:false
    }).setView([42.7, 25.5], 7);

    $('.leaflet-container').css('cursor', 'crosshair');
    map.setMaxBounds(bounds);
    map.on('drag', function () {
        map.panInsideBounds(bounds, {animate: false});
    });

    map.on('movestart', function () {
        $('.leaflet-container').css('cursor', '');
    });

    map.on('moveend', function () {
        $('.leaflet-container').css('cursor', 'crosshair');
    });
    //CURSOR CONTROL END

    //SHAPE STYLING START
    var shapeStyle = {
        "color": "#000000",
        "weight": 2,
        "fillOpacity": 0.6,
        "opacity": 0.75
    };
    //SHAPE STYLING END

    //COLORING START
    const colors = new Map(
        [
            ['A', '#ff0000'], ['B', '#b26559'], ['C', '#f28100'], ['D', '#a7b300'], ['E', '#ffff00'], ['F', '#005359'],
            ['G', '#3d55f2'], ['H', '#ff00cc'], ['I', '#ff80a2'], ['J', '#cc0000'], ['K', '#733f1d'], ['L', '#736739'],
            ['M', '#134d13'], ['N', '#26332d'], ['O', '#0058a6'], ['P', '#140033'], ['Q', '#99738c'], ['R', '#331a1a'],
            ['S', '#42ceb6'], ['T', '#fff2bf'], ['U', '#79f279'], ['V', '#00becc'], ['W', '#bfd9ff'], ['X', '#660080'],
            ['Y', '#021412'], ['Z', '#992663']
        ]
    );

    const colorEarthquakeArray = ['#ff0000', '#b26559', '#ff80a2', '#00becc', '#bfd9ff',
        '#021412', '#e31a1c', '#992663', '#800026', '#720413'];

    function getColorBySoilGroup(obj) {
        return colors.get(obj.properties.domsoi.charAt(0));
    }

    function getColorByEQFrequency(obj) {
        return colorEarthquakeArray[obj.properties.dn - 1];
    }

    function getColor(obj) {
        if (obj.properties.domsoi !== undefined) {
            return getColorBySoilGroup(obj);
        } else if (obj.properties.dn !== undefined) {
            return getColorByEQFrequency(obj);
        }
    }

    function style(obj) {
        return {
            fillColor: getColor(obj),
            weight: 2,
            opacity: 1,
            color: 'white',
            dashArray: '3',
            fillOpacity: 0.5
        };
    }

    map.on("contextmenu", function () {
        layerGroup.clearLayers();
        info.update();
        if (marker !== null) {
            map.removeLayer(marker);
        }
    });

    var layerGroup = L.layerGroup().addTo(map);
    //AJAX START

    var geojson;


    function updateInfoOnClick(e) {
        info.update(layer.feature)
    }

    function onEachFeature(feature, layer) {
        layer.on({
            click: updateInfoOnClick,
        });
    }

    map.on("dblclick", function (ev) {

        var latlng = map.mouseEventToLatLng(ev.originalEvent);

        latX = latlng.lng;
        latY = latlng.lat;

        currentShape = $("select.shape-selector option:selected").val();

        $.ajax({
            url: "http://localhost:8080/spatial/getByCoords",
            type: "POST",
            dataType: "text",
            contentType: "application/json",
            data: JSON.stringify({
                "x": latX,
                "y": latY,
                "requestType": currentShape
            }),
            success: function (response) {

                // layerGroup.clearLayers();

                geojson = $.parseJSON(response);

                shapeStyle.fillColor = getColor(geojson);

                var geometry = wellknown.parse(geojson.geometry);
                geojson.geometry = geometry;

                geojson = L.geoJSON(geojson, {
                    style: style,
                    onEachFeature: onEachFeature
                });

                // printPropertiesOnConsole(geojson.properties);

                geojson.addTo(layerGroup,);

                // info.update(geojson);
            },
            error: function (response) {
                var errorMessage = jQuery.parseJSON(response.responseText);
                console.log(errorMessage.message);
                if (layer !== null) {
                    map.removeLayer(layer);
                }
                info.updateOnError();
                // popup.setContent('No data found!')
            }
        });

        $.ajax({
            url: "/api/spatial/getRequestPointData",
            type: "POST",
            dataType: "text",
            contentType: "application/json",
            data: JSON.stringify({
                "x": latX,
                "y": latY,
                "requestType": currentShape
            }),


        });
    });
    //AJAX END

    layerGroup.on('dbclick', function (e) {
        info.update(e);

    });




    //DROP-DOWN START
    $.ajax({
        url: "http://localhost:8080/spatial/getRequestTypes",
        type: "POST",
        dataType: "text",
        contentType: "application/json",
        success: function (response) {
            var requests = $.parseJSON(response);

            var html_dropdown = "";
            if (requests.length !== 0) {
                html_dropdown += "<option value=\"" + requests[0] + "\" selected> " + requests[0] + " </option>";
                for (var i = 1; i < requests.length; i++) {
                    html_dropdown += "<option value=\"" + requests[i] + "\"> " + requests[i] + " </option>";
                }
            } else {
                html_dropdown += "<option value=\"\" disabled selected>No shapes</option>";
            }
            $("#requestType").append(html_dropdown);
            // $('select').formSelect();
            currentShape = $("select.shape-selector option:selected").val();
        }
    });
    //DROP-DOWN END


    //LEAFLET START
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
        maxZoom: 18,
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
            '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
            'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        id: 'mapbox.streets'
    }).addTo(map);

    map.doubleClickZoom.disable();

    var popup = L.popup();

    function onMapClick(e) {
        if (marker !== null) {
            map.removeLayer(marker);
        }

        marker = L.marker(e.latlng).addTo(map);

        // popup
        //     .setLatLng(e.latlng)
        //     .setContent('<b>X: ' + parseFloat(Math.round(e.latlng.lng * 100000) / 100000).toFixed(5).toString() + '</b></br> + ')
        //     .openOn(map);
    }

    //LEAFLET END


    //INFO CONTROL START
    var info = L.control();

    info.onAdd = function (map) {
        this._div = L.DomUtil.create('div', 'info'); // create a div with a class "info"
        this.update();
        return this._div;
    };

// method that we will use to update the control based on feature properties passed


    // info.update = function (obj) {
    //     if (currentShape === soilShape) {
    //         this._div.innerHTML = '<h2><u><span class=capitalize>' + currentShape + ' Information</span></u></h2>';
    //         for(prop in obj.properties){
    //             console.log(prop);
    //             console.log(obj.properties[prop]);
    //         }
    //         this._div.innerHTML += (obj ?
    //             '<h3>Faosoil: ' + obj.properties.faosoil + '</h3>' +
    //             '<h3>Domsoi: ' + obj.properties.domsoi + '</h3>' +
    //             '<h3>Country: <span class = capitalize>' + obj.properties.country.toLowerCase() + '</span></h3>' +
    //             '<h3>Size: ' + obj.properties.sqkm + ' km<sup>2</sup></h3>'
    //             : '<h3>Double click on location...</h3>');
    //     } else if (currentShape === earthquakeShape) {
    //         this._div.innerHTML = '<h2><u><span class=capitalize>' + currentShape + ' Information</span></u></h2>';
    //
    //         this._div.innerHTML += (obj ?
    //             '<span>' + '<h3>Dn: ' + obj.properties.dn + '</h3></span>'
    //             : '<h3>Double click on location...</h3>');
    //     }
    // };

    info.update = function (obj) {
        if (currentShape !== null) {
            this._div.innerHTML = '<h2><u><span class=capitalize>' + currentShape + ' Information</span></u></h2>';

            if (obj) {

                let toAdd = "";

                for (prop in obj.properties) {
                    toAdd += '<h3><span class=capitalize>' + prop + ': ' + obj.properties[prop] + '</span></h3>';
                }

                this._div.innerHTML += toAdd;

            } else {
                this._div.innerHTML += '<h3>Double click on location...</h3>';
            }
        }
    };


    info.updateOnError = function () {
        this._div.innerHTML = '<h2><u><span class=capitalize>' + currentShape + ' Information</span></u></h2>';
        this._div.innerHTML += '<h3>Double click on location...</h3>';
    };

    map.on('dblclick', onMapClick);
    info.addTo(map);
    //INFO CONTROL END

    //HIGHLIGHT START
    function highlightFeature(e) {
        var layer = e.target;

        layer.setStyle({
            weight: 4,
            color: '#FFFF00',
            dashArray: '',
            fillOpacity: 0.8
        });

        if (!L.Browser.ie && !L.Browser.opera && !L.Browser.edge) {
            layer.bringToFront();
        }

        info.update(layer.feature);
    }

    function resetHighlight(e) {
        geojson.resetStyle(e.target);
        info.update();
    }

    function onEachFeature(feature, layer) {
        layer.on({
            mouseover: highlightFeature,
            mouseout: resetHighlight,
        });
    }

});