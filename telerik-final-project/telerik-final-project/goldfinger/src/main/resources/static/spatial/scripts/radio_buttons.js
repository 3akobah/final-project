$.ajax({
    url: "http://localhost:8080/spatial/getRequestTypes",
    type: "POST",
    dataType: "text",
    contentType: "application/json",
    success: function (response) {
        let obj = $.parseJSON(response);

        let html_radioButtons = "<div style=\"display: inline-block\"><input class=\"with-gap\" type=\"radio\" id=\"radio0" + 1 +
            "\" checked value=\"" + obj[0] + "\" name=\"group1\"/>" +
            "<label for=\"radio0" + 1 + "\"><span></span>" + obj[0].toUpperCase() + "</label></div>";

        for (let i = 1; i < obj.length; i++) {
            html_radioButtons += "<div style=\"display: inline-block\"><input class=\"with-gap\" type=\"radio\" id=\"radio0" + (i + 1) +
                "\" value=\"" + obj[i] + "\" name=\"group1\"/>" +
                "<label for=\"radio0" + (i + 1) + "\"><span></span>" + obj[i].toUpperCase() + "</label></div>";
        }

        $("#requestType").html(html_radioButtons);
    }
});