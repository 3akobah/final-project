package com.team5.spatial.helpers.GeometryParser;

import org.junit.Assert;
import org.junit.Test;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.io.WKBWriter;
import org.locationtech.jts.io.WKTReader;

import java.io.ByteArrayInputStream;

public class GeometryParserImplTest {
    private GeometryParserImpl geometryParserImpl = new GeometryParserImpl();

    @Test
    public void testParseGeometry() throws Exception {
        //Arrange
        final WKTReader reader = new WKTReader();
        final String polygonAsWKT = "POLYGON((20 10, 30 0, 40 10, 30 20, 20 10))";
        Geometry expected = reader.read(polygonAsWKT);
        WKBWriter writer = new WKBWriter();

        //WKBWriter writes geometry to byte[] with no SRID
        byte[] polygonAsByteArray = writer.write(expected);

        //SRID's location is in the first 4 bites. The GeometryParser needs SRID to be defined.
        byte[] polygonAsByteArrayWithSRID = new byte[polygonAsByteArray.length + 4];
        System.arraycopy(polygonAsByteArray, 0, polygonAsByteArrayWithSRID, 4, polygonAsByteArray.length);

        //Act
        Geometry result = geometryParserImpl.parseGeometry(new ByteArrayInputStream(polygonAsByteArrayWithSRID));

        //Assert
        Assert.assertEquals(expected.toString(), result.toString());
    }
}