package com.team5.spatial.helpers;

import org.junit.Test;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.ResultSet;
import static org.mockito.Mockito.*;


public class ResultSetHelperTest {

    private ResultSet resultSet = mock(ResultSet.class);

    @Test(expected = EmptyResultDataAccessException.class)
    public void testCheckIfEmpty() throws Exception {

        when(resultSet.isBeforeFirst()).thenReturn(false);

        ResultSetHelper.checkIfEmpty(resultSet);
    }

}
