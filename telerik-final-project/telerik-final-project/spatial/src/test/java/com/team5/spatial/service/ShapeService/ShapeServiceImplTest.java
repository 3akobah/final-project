package com.team5.spatial.service.ShapeService;

import com.team5.spatial.helpers.GeometryParser.GeometryParser;
import com.team5.spatial.models.Request;
import com.team5.spatial.repository.ShapeRepository.ShapeRepository;
import com.team5.spatial.service.TableNameService.RequestTypeService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.locationtech.jts.io.WKTReader;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

public class ShapeServiceImplTest {
    @Mock
    ShapeRepository shapeRepository;
    @Mock
    RequestTypeService requestTypeService;
    @Mock
    GeometryParser geometryParser;
    @Mock
    Request request;

    @InjectMocks
    ShapeServiceImpl shapeServiceImpl;


    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetShapeSuccess() throws Exception {
        //Arrange
        String wkt = "POLYGON ((-125 38.4, -125 40.9, -121.8 40.9, -121.8 38.4, -125 38.4))";
        WKTReader writer = new WKTReader();
        when(geometryParser.parseGeometry(any())).thenReturn(writer.read(wkt));

        when(shapeRepository.getShape(any())).thenReturn(new HashMap() {{
            put("geometry", new byte[5]);
        }});

        //Act
        Map result = shapeServiceImpl.getShape(request);

        //Assert
        Assert.assertEquals(wkt, result.get("geometry"));
    }


    @Test(expected = IllegalArgumentException.class)
    public void testGetShapeInvalidRequest() throws Exception {
        doThrow(IllegalArgumentException.class).when(requestTypeService).checkIfValid(any());

        Map result = shapeServiceImpl.getShape(request);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testGetShapeNoResult() throws Exception {
        doThrow(EmptyResultDataAccessException.class).when(shapeRepository).getShape(any());

        Map result = shapeServiceImpl.getShape(request);
    }

}
