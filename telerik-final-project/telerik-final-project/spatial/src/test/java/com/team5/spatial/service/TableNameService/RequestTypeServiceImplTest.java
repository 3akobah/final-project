package com.team5.spatial.service.TableNameService;

import com.team5.spatial.repository.RequestTypeRepository.RequestTypeRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.dao.EmptyResultDataAccessException;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;

public class RequestTypeServiceImplTest {
    @Mock
    RequestTypeRepository requestTypeRepository;
    @InjectMocks
    RequestTypeServiceImpl requestTypeServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetTableNames() throws SQLException {
        when(requestTypeRepository.getTableNames()).thenReturn(Arrays.asList("TableName1", "TableName2"));

        List<String> result = requestTypeServiceImpl.getTableNames();
        Assert.assertEquals(Arrays.asList("TableName1", "TableName2"), result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIfValidInvalidTableName() throws SQLException{
        when(requestTypeRepository.getTableNames()).thenReturn(Arrays.asList("ValidName1", "ValidName2"));

        requestTypeServiceImpl.checkIfValid("InvalidName");
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void getTableNamesEmptyResult() throws SQLException{
        when(requestTypeRepository.getTableNames()).thenReturn(Arrays.asList("ValidName1", "ValidName2"));

        doThrow(EmptyResultDataAccessException.class).when(requestTypeRepository).getTableNames();

        requestTypeServiceImpl.getTableNames();
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCheckIfValidInvalidSqlInjection(){
        String sqlInjection = "'SELECT FROM'";
        requestTypeServiceImpl.checkIfValid(sqlInjection);
    }
}