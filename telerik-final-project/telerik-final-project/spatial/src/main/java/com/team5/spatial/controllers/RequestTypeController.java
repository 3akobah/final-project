package com.team5.spatial.controllers;

import com.team5.spatial.service.TableNameService.RequestTypeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Api(value="Spatial Management System", description="Operations pertaining to request types in the Spatial Management System")
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/spatial")
public class RequestTypeController {

    private RequestTypeService requestTypeService;

    @Autowired
    public RequestTypeController(RequestTypeService requestTypeService) {
        this.requestTypeService = requestTypeService;
    }

    @ApiOperation(value = "Get the table names containing GIS data.", response = List.class)
    @CrossOrigin(origins = "http://localhost:8090")
    @PostMapping("/getRequestTypes")
    public List getTableLames(){
        try{
            return requestTypeService.getTableNames();
        }catch (EmptyResultDataAccessException e){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }catch (DataAccessResourceFailureException e){
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
        }catch (IllegalArgumentException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}