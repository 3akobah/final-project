package com.team5.spatial.repository.RequestTypeRepository;

import java.sql.SQLException;
import java.util.List;

public interface RequestTypeRepository {

    List<String> getTableNames() throws SQLException;
}
