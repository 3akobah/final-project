package com.team5.spatial.helpers.GeometryParser;

import org.locationtech.jts.geom.Geometry;

import java.io.InputStream;

public interface GeometryParser {
    Geometry parseGeometry(InputStream inputStream) throws Exception;
}
