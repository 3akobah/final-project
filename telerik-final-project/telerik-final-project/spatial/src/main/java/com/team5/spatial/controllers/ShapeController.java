package com.team5.spatial.controllers;

import com.team5.spatial.models.Request;
import com.team5.spatial.service.ShapeService.ShapeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Map;

@Api(value = "Spatial Management System", description = "Operations pertaining to shapes in the Spatial Management System")
@CrossOrigin(origins = "http://localhost:8090", maxAge = 3600)
@RestController
@RequestMapping("/spatial")
public class ShapeController {

    private ShapeService shapeService;

    @Autowired
    public ShapeController(ShapeService shapeService) {
        this.shapeService = shapeService;
    }

    @ApiOperation(value = "Get the GIS data with a geometry containing the request Point", response = Map.class)
    @PostMapping("/getByCoords")
    public Map getShape(@RequestBody Request point) throws Exception {
        try {
            return shapeService.getShape(point);
        } catch (EmptyResultDataAccessException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DataAccessResourceFailureException e) {
            throw new ResponseStatusException(HttpStatus.SERVICE_UNAVAILABLE, e.getMessage());
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
