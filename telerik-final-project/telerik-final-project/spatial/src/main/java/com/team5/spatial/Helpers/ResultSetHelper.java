package com.team5.spatial.helpers;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ResultSetHelper {
    private static final String NO_RESULT_FOUND = "No data found for this request...";

    public static void checkIfEmpty(ResultSet resultSet) throws SQLException {
        if (!resultSet.isBeforeFirst()) {
            throw new EmptyResultDataAccessException(NO_RESULT_FOUND, 1);
        }
    }

    public static List toList(ResultSet resultSet) throws SQLException {
        List result = new ArrayList();

        int columns = resultSet.getMetaData().getColumnCount();
        while (resultSet.next()) {
            for (int i = 1; i <= columns; i++) {
                result.add(resultSet.getObject(i).toString());
            }
        }

        return result;
    }

    public static Map toMap(ResultSet resultSet) throws SQLException {
        ResultSetMetaData md = resultSet.getMetaData();
        int columns = md.getColumnCount();

        Map result = new HashMap<>();
        Map properties = new HashMap<>();

        while (resultSet.next()) {
            for (int i = 1; i <= columns; i++) {
                if (md.getColumnName(i).equals("SHAPE")) {
                    result.put("geometry", resultSet.getObject(i));
                } else {
                    properties.put(md.getColumnName(i), resultSet.getObject(i));
                }
            }
        }

        result.put("properties", properties);
        return result;
    }
}