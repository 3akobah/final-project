package com.team5.spatial.service.TableNameService;

import java.util.List;

public interface RequestTypeService {
    List<String> getTableNames();
    void checkIfValid(String tableName);
}
