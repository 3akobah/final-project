package com.team5.spatial.models;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "All details about the Request.")
public class Request {
    private double x;
    private double y;
    private String requestType;

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }
}
