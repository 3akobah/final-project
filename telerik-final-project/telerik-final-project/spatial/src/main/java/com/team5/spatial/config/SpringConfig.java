package com.team5.spatial.config;

import com.team5.spatial.helpers.GeometryParser.GeometryParserImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SpringConfig {
    @Bean
    GeometryParserImpl geometryParser() {
        return new GeometryParserImpl();
    }
}
