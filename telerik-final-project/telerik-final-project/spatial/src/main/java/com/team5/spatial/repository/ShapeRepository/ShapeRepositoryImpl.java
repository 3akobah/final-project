package com.team5.spatial.repository.ShapeRepository;

import com.team5.spatial.helpers.ResultSetHelper;
import com.team5.spatial.models.Request;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Map;


@Repository
@PropertySource("classpath:application.properties")
public class ShapeRepositoryImpl implements ShapeRepository {
    private final String NO_RESULT_FOUND = "No data found for this location...";
    private String BAD_REQUEST = "Request for %s could not return 'shape' form...";
    private String dbUrl, username, password;

    @Autowired
    ShapeRepositoryImpl(Environment env) {
        dbUrl = env.getProperty("database.url");
        username = env.getProperty("database.username");
        password = env.getProperty("database.password");
    }

    @Override
    public Map getShape(Request request) throws Exception {

        String query = "SELECT * FROM " + request.getRequestType() + " WHERE CONTAINS(SHAPE,Point(? ,?));";

        try (
                Connection connection = DriverManager.getConnection(dbUrl, username, password);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {
            statement.setDouble(1, request.getX());
            statement.setDouble(2, request.getY());

            ResultSet propertiesSet = statement.executeQuery();

            ResultSetHelper.checkIfEmpty(propertiesSet);

            return ResultSetHelper.toMap(propertiesSet);
        } catch (SQLException e) {
            String message = String.format(BAD_REQUEST, request.getRequestType());
            System.out.println(message);
            throw new SQLException(message);
        } catch (EmptyResultDataAccessException e) {
            throw new EmptyResultDataAccessException(NO_RESULT_FOUND, e.getActualSize());
        }
    }
}