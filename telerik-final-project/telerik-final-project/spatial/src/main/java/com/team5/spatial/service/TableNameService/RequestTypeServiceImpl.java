package com.team5.spatial.service.TableNameService;

import com.team5.spatial.repository.RequestTypeRepository.RequestTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.sql.SQLException;
import java.util.List;

@Service
public class RequestTypeServiceImpl implements RequestTypeService {

    private RequestTypeRepository requestTypeRepository;
    private final String REQUEST_NOT_VALID = "Request not valid!" ;

    @Autowired
    public RequestTypeServiceImpl(RequestTypeRepository requestTypeRepository) {
        this.requestTypeRepository = requestTypeRepository;
    }

    @Override
    public List<String> getTableNames() {

        try {
            return requestTypeRepository.getTableNames();
        } catch (EmptyResultDataAccessException e) {
            throw new EmptyResultDataAccessException(e.getMessage(), e.getActualSize());
        }
        catch (SQLException e){
            throw new DataAccessResourceFailureException(e.getMessage());
        }
    }

    @Override
    public void checkIfValid(String tableName) {
            if (!getTableNames().contains(tableName)){
                if(!tableName.matches("[a-zA-Z0-9]*")) {
                    System.out.println("Possible SQL injection attempt!");
                }
                throw new IllegalArgumentException(REQUEST_NOT_VALID);
            }
    }
}
