package com.team5.spatial.service.ShapeService;

import com.team5.spatial.helpers.GeometryParser.GeometryParser;
import com.team5.spatial.models.Request;
import com.team5.spatial.repository.ShapeRepository.ShapeRepository;

import com.team5.spatial.service.TableNameService.RequestTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessResourceFailureException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Map;

@Service
public class ShapeServiceImpl implements ShapeService {
    private ShapeRepository shapeRepository;
    private RequestTypeService requestTypeService;
    private GeometryParser geometryParser;

    private final String DATA_BASE_PROBLEM = "Database problem occurred...";
    private final String PARSE_FAILIURE = "Failed to parse shape!";

    @Autowired
    public ShapeServiceImpl(ShapeRepository shapeRepository, GeometryParser geometryParser, RequestTypeService requestTypeService) {
        this.shapeRepository = shapeRepository;
        this.geometryParser = geometryParser;
        this.requestTypeService = requestTypeService;
    }

    @Override
    public Map getShape(Request request) throws Exception {

        try {
            requestTypeService.checkIfValid(request.getRequestType());
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException(e.getMessage());
        }

        try {
            Map shapeResult = shapeRepository.getShape(request);
            try {
                byte[] shapeAsByteArray = (byte[]) shapeResult.get("geometry");

                String geometryWKT = geometryParser.parseGeometry(new ByteArrayInputStream(shapeAsByteArray)).toString();
                shapeResult.put("type", "Feature");
                shapeResult.put("geometry", geometryWKT);
            } catch (ParseException | IOException e) {
                throw new IllegalArgumentException(PARSE_FAILIURE);
            }
            return shapeResult;
        }  catch (EmptyResultDataAccessException e) {
            throw new EmptyResultDataAccessException(e.getMessage(), e.getActualSize());
        } catch (SQLException e){
            throw new DataAccessResourceFailureException(DATA_BASE_PROBLEM) ;
        }
    }
}
