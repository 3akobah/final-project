package com.team5.spatial.repository.RequestTypeRepository;

import com.team5.spatial.helpers.ResultSetHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@PropertySource("classpath:application.properties")
public class RequestTypeRepositoryImpl implements RequestTypeRepository {
    private final String NO_TABLES_FOUND = "No tables found";
    private String dbUrl, username, password;

    @Autowired
    RequestTypeRepositoryImpl(Environment env) {
        dbUrl = env.getProperty("database.url");
        username = env.getProperty("database.username");
        password = env.getProperty("database.password");
    }

    @Override
    public List getTableNames() throws SQLException{

        String query = "SELECT DISTINCT TABLE_NAME \n" +
                " FROM INFORMATION_SCHEMA.COLUMNS\n" +
                " WHERE COLUMN_NAME IN ('SHAPE')\n" +
                " AND TABLE_SCHEMA=(SELECT DATABASE());";
        try (
                Connection connection = DriverManager.getConnection(dbUrl, username, password);
                PreparedStatement statement = connection.prepareStatement(query)
        ) {

            ResultSet tableNamesSet = statement.executeQuery();

            ResultSetHelper.checkIfEmpty(tableNamesSet);

            return ResultSetHelper.toList(tableNamesSet);

        } catch (SQLException e) {
            System.out.println("SQL Exception at " + this.getClass() + " in method getTableNames()");
            throw new SQLException("Database problem occurred...");
        } catch (EmptyResultDataAccessException e) {
            throw new EmptyResultDataAccessException(NO_TABLES_FOUND, e.getActualSize());
        }
    }
}
