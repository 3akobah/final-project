package com.team5.spatial.repository.ShapeRepository;

import com.team5.spatial.models.Request;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Map;

public interface ShapeRepository {

    Map getShape(@RequestBody Request point) throws Exception ;
}
