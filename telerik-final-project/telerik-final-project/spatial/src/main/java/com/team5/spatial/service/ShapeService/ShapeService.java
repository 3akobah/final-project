package com.team5.spatial.service.ShapeService;

import com.team5.spatial.models.Request;

import java.util.Map;

public interface ShapeService {
    Map getShape(Request point) throws Exception;
}
