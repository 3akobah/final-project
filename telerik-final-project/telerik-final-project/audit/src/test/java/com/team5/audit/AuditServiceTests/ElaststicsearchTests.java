package com.team5.audit.AuditServiceTests;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.team5.audit.models.LogEntry;
import com.team5.audit.services.AuditServiceImpl;
import org.apache.commons.logging.impl.Log4JLogger;
import org.apache.http.HttpHost;
import org.apache.logging.log4j.Logger;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.test.ESTestCase;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Value;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.team5.audit.config.Constant.INDEX;
import static com.team5.audit.config.Constant.TYPE;

@RunWith(MockitoJUnitRunner.class)
public class ElaststicsearchTests  {

    private static String INDEX = "audit5";
    private static String TYPE = "logEntry";



    @Mock
    RestHighLevelClient client;

    @Mock
    ObjectMapper objectMapper;

    @Mock
    Logger logger;

    @InjectMocks
    AuditServiceImpl auditService;

    @Before
    public void setUp() {
        client = new RestHighLevelClient(
                RestClient.builder(new HttpHost("search-movies-mfhq3ooghrqxmmrgjwkzroqh4q.us-east-2.es.amazonaws.com"))
        );
    }

    @Test
    public void createLogEntry_Should_ReturnCreated_When_Successful() throws Exception{

        LogEntry logEntry = new LogEntry();

        logEntry.setUsername("marin");
        logEntry.setIp("1.1.1.1");

        Map<String, Object> details = new HashMap<>();
        details.put("event", "Login");
        logEntry.setDetails(details);

        UUID uuid = UUID.randomUUID();
        logEntry.setId(uuid.toString());
        Map<String, Object> document = objectMapper.convertValue(logEntry, Map.class);

        IndexRequest indexRequest = new IndexRequest(INDEX, TYPE, logEntry.getId())
                .source(document);

        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);





        String index = indexResponse.getIndex();
        String type = indexResponse.getType();

//        Mockito.when(auditService.createLogEntry(logEntry))
//                .thenReturn("CREATED");
//        String result = auditService.createLogEntry(logEntry);

        //Assert.assertEquals("CREATED", indexResponse.getResult().toString());
        Assert.assertEquals("audit5", index);

    }


    @Test
    public void testSearch() throws Exception {


    }

}
