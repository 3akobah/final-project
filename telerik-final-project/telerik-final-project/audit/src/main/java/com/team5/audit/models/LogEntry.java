package com.team5.audit.models;


//import io.swagger.annotations.ApiModel;

import java.util.HashMap;
import java.util.Map;

//@ApiModel(description = "Details about the log entry.")
public class LogEntry {


    private String id;

    private String username;

    private String ip;

    private String timestamp;

    private Map<String, Object> details;

    public LogEntry() {
    }

    public LogEntry(String id, String username, String ip, String timestamp) {
        this.id = id;
        this.username = username;
        this.ip = ip;
        this.timestamp = timestamp;
        this.details = new HashMap<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

    public void setDetails(Map<String, Object> details) {
        this.details = details;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}
