package com.team5.audit.services;

import com.team5.audit.models.LogEntry;
import com.team5.audit.models.SearchDto;


import java.util.List;

public interface AuditService {

    String createLogEntry(LogEntry logEntry) throws Exception;

    List<LogEntry> findAll() throws Exception;

    String deleteLogEntry(String id) throws Exception;

    List<LogEntry> search(SearchDto searchDto) throws Exception;
}
