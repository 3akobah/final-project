package com.team5.audit.config;

public interface Constant {
    String INDEX = "audit6";
    String TYPE = "logEntry";

    String noMessageTemplate = "{\n" +
            "  \"query\": {\n" +
            "    \"bool\": {\n" +
            "      \"filter\": {\n" +
            "        {{#timestamp}}\n" +
            "          \"range\": {\n" +
            "            \"timestamp\": {\n" +
            "        {{#start}}\n" +
            "        \"gte\": \"{{start}}\"\n" +
            "        {{#end}},{{/end}}\n" +
            "        {{/start}}\n" +
            "        {{#end}}\n" +
            "        \"lte\": \"{{end}}\"\n" +
            "        {{/end}}\n" +
            "      }\n" +
            "          }\n" +
            "        {{/timestamp}}\n" +
            "        }\n" +
            "    }\n" +
            "  },\n" +
            "   \"size\" : 9999,\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"timestamp\": {\n" +
            "        \"order\": \"desc\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}\n" +
            "\n" +
            "\n";

    String anyTemplate = "{\n" +
            "  \"query\": {\n" +
            "    \"bool\": {\n" +
            "      \"must\": [\n" +
            "        {\n" +
            "          \"query_string\": {\n" +
            "            \"query\": \"{{message}}\"\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"filter\": {\n" +
            "        {{#timestamp}}\n" +
            "          \"range\": {\n" +
            "            \"timestamp\": {\n" +
            "        {{#start}}\n" +
            "        \"gte\": \"{{start}}\"\n" +
            "        {{#end}},{{/end}}\n" +
            "        {{/start}}\n" +
            "        {{#end}}\n" +
            "        \"lte\": \"{{end}}\"\n" +
            "        {{/end}}\n" +
            "      }\n" +
            "          }\n" +
            "        {{/timestamp}}\n" +
            "        }\n" +
            "    }\n" +
            "  },\n" +
            "   \"size\" : 9999,\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"timestamp\": {\n" +
            "        \"order\": \"desc\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}\n" +
            "\n" +
            "\n";

    String phraseTemplate = "{\n" +
            "  \"query\": {\n" +
            "    \"bool\": {\n" +
            "      \"must\": [\n" +
            "        {\n" +
            "          \"multi_match\" : {\n" +
            "            \"query\":    \"{{message}}\",\n" +
            "            \"type\" : \"phrase\"\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"filter\": {\n" +
            "        {{#timestamp}}\n" +
            "          \"range\": {\n" +
            "            \"timestamp\": {\n" +
            "        {{#start}}\n" +
            "        \"gte\": \"{{start}}\"\n" +
            "        {{#end}},{{/end}}\n" +
            "        {{/start}}\n" +
            "        {{#end}}\n" +
            "        \"lte\": \"{{end}}\"\n" +
            "        {{/end}}\n" +
            "      }\n" +
            "          }\n" +
            "        {{/timestamp}}\n" +
            "        }\n" +
            "    }\n" +
            "  },\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"timestamp\": {\n" +
            "        \"order\": \"desc\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}\n"
            ;

    String keyAnyTemplate = "{\n" +
            "  \"query\": {\n" +
            "    \"bool\": {\n" +
            "      \"must\": [\n" +
            "        {\n" +
            "          \"multi_match\" : {\n" +
            "            \"query\":    \"{{message}}\",\n" +
            "            \"fields\": [ \"{{field}}\" ]\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"filter\": {\n" +
            "        {{#timestamp}}\n" +
            "          \"range\": {\n" +
            "            \"timestamp\": {\n" +
            "        {{#start}}\n" +
            "        \"gte\": \"{{start}}\"\n" +
            "        {{#end}},{{/end}}\n" +
            "        {{/start}}\n" +
            "        {{#end}}\n" +
            "        \"lte\": \"{{end}}\"\n" +
            "        {{/end}}\n" +
            "      }\n" +
            "          }\n" +
            "        {{/timestamp}}\n" +
            "        }\n" +
            "    }\n" +
            "  },\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"timestamp\": {\n" +
            "        \"order\": \"desc\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}\n" ;

    String keyPhraseTemplate = "{\n" +
            "  \"query\": {\n" +
            "    \"bool\": {\n" +
            "      \"must\": [\n" +
            "        {\n" +
            "          \"multi_match\" : {\n" +
            "            \"query\":    \"{{message}}\",\n" +
            "            \"type\" : \"phrase\",\n" +
            "            \"fields\": [ \"{{field}}\" ]\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"filter\": {\n" +
            "        {{#timestamp}}\n" +
            "          \"range\": {\n" +
            "            \"timestamp\": {\n" +
            "        {{#start}}\n" +
            "        \"gte\": \"{{start}}\"\n" +
            "        {{#end}},{{/end}}\n" +
            "        {{/start}}\n" +
            "        {{#end}}\n" +
            "        \"lte\": \"{{end}}\"\n" +
            "        {{/end}}\n" +
            "      }\n" +
            "          }\n" +
            "        {{/timestamp}}\n" +
            "        }\n" +
            "    }\n" +
            "  },\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"timestamp\": {\n" +
            "        \"order\": \"desc\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}\n"
            ;

    String keyExactTemplate = "{\n" +
            "  \"query\": {\n" +
            "    \"bool\": {\n" +
            "      \"must\": [\n" +
            "        {\n" +
            "          \"term\": {\n" +
            "            \"{{field}}\": \"{{message}}\"\n" +
            "          }\n" +
            "        }\n" +
            "      ],\n" +
            "      \"filter\": [\n" +
            "        { {{#timestamp}}\n" +
            "          \"range\": {\n" +
            "            \"timestamp\": {\n" +
            "        {{#start}}\n" +
            "        \"gte\": \"{{start}}\"\n" +
            "        {{#end}},{{/end}}\n" +
            "        {{/start}}\n" +
            "        {{#end}}\n" +
            "        \"lte\": \"{{end}}\"\n" +
            "        {{/end}}\n" +
            "      }\n" +
            "          }\n" +
            "        {{/timestamp}}\n" +
            "        }\n" +
            "      ]\n" +
            "    }\n" +
            "  },\n" +
            "   \"size\" : 9999,\n" +
            "  \"sort\": [\n" +
            "    {\n" +
            "      \"timestamp\": {\n" +
            "        \"order\": \"desc\"\n" +
            "      }\n" +
            "    }\n" +
            "  ]\n" +
            "}"
            ;

}
