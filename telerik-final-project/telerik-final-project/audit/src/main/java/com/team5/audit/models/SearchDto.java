package com.team5.audit.models;

//import io.swagger.annotations.ApiModel;

//@ApiModel(description = "Details about the search DTO.")
public class SearchDto {

    private String message;
    private String startDate;
    private String endDate;

    public SearchDto() {
    }

    public SearchDto(String message, String startDate, String endDate) {
        this.message = message;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
}
