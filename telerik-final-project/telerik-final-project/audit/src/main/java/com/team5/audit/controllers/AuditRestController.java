package com.team5.audit.controllers;

import com.team5.audit.models.LogEntry;
import com.team5.audit.models.SearchDto;
import com.team5.audit.services.AuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@Api(value="Goldfinger Auditing System", description="Operations pertaining to auditing in the Goldfinger Management System")
@CrossOrigin(origins = "http://localhost:8090", maxAge = 3600)
@RestController
@RequestMapping("/api/audit/")
public class AuditRestController {
    
    private AuditService auditService;
    
    @Autowired
    public AuditRestController(AuditService auditService) {
        this.auditService = auditService;
    }

    @ApiOperation(value = "Add new log.", response = ResponseEntity.class)
    @PostMapping("/addNew")
    public ResponseEntity createLogEntry(
            @RequestBody LogEntry logEntry) throws Exception {

        return
                new ResponseEntity(auditService.createLogEntry(logEntry), HttpStatus.CREATED);
    }

    @GetMapping("/all")
    public List<LogEntry> findAll() throws Exception {

        return auditService.findAll();
    }

    @DeleteMapping("/delete/{id}")
    public String deleteOne(@PathVariable String id) throws Exception {

        return auditService.deleteLogEntry(id);
    }

    @ApiOperation(value = "Make a search.", response = List.class)
    @PostMapping("/search/do-search")
    public List<LogEntry> search(@RequestBody SearchDto searchDto) throws Exception {

        return auditService.search(searchDto);
    }
}
