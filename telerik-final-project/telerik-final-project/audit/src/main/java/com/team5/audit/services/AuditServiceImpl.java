package com.team5.audit.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.team5.audit.models.LogEntry;
import com.team5.audit.models.SearchDto;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.index.query.MatchQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.script.ScriptType;
import org.elasticsearch.script.mustache.SearchTemplateRequest;
import org.elasticsearch.script.mustache.SearchTemplateResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.*;

import static com.team5.audit.config.Constant.*;

@Service
public class AuditServiceImpl implements AuditService {


    private RestHighLevelClient client;

    private ObjectMapper objectMapper;

    @Autowired
    public AuditServiceImpl(RestHighLevelClient client, ObjectMapper objectMapper) {
        this.client = client;
        this.objectMapper = objectMapper;
    }

    @Override
    public String createLogEntry(LogEntry logEntry) throws Exception {

        UUID uuid = UUID.randomUUID();
        logEntry.setId(uuid.toString());

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        logEntry.setTimestamp(formatter.format(date));

        IndexRequest indexRequest = new IndexRequest(INDEX, TYPE, logEntry.getId())
                .source(convertLogEntryToMap(logEntry));

        IndexResponse indexResponse = client.index(indexRequest, RequestOptions.DEFAULT);
        return indexResponse.getResult().name();
    }

    @Override
    public List<LogEntry> findAll() throws Exception {

        SearchRequest searchRequest = buildSearchRequest(INDEX,TYPE);
        SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
        searchSourceBuilder.query(QueryBuilders.matchAllQuery());
        searchRequest.source(searchSourceBuilder);

        SearchResponse searchResponse =
                client.search(searchRequest, RequestOptions.DEFAULT);

        return getSearchResult(searchResponse);
    }


    @Override
    public String deleteLogEntry(String id) throws Exception {

        DeleteRequest deleteRequest = new DeleteRequest(INDEX, TYPE, id);
        DeleteResponse response = client.delete(deleteRequest,RequestOptions.DEFAULT);

        return response
                .getResult()
                .name();

    }

    private Map<String, Object> convertLogEntryToMap(LogEntry LogEntry) {
        return objectMapper.convertValue(LogEntry, Map.class);
    }

    private LogEntry convertMapToLogEntry(Map<String, Object> map){
        return objectMapper.convertValue(map,LogEntry.class);
    }


    private List<LogEntry> getSearchResult(SearchResponse response) {

        SearchHit[] searchHit = response.getHits().getHits();

        List<LogEntry> LogEntries = new ArrayList<>();

        for (SearchHit hit : searchHit){
            LogEntries
                    .add(objectMapper
                            .convertValue(hit
                                    .getSourceAsMap(), LogEntry.class));
        }

        return LogEntries;
    }

    private SearchRequest buildSearchRequest(String index, String type) {

        SearchRequest searchRequest = new SearchRequest();
        searchRequest.indices(index);
        searchRequest.types(type);

        return searchRequest;
    }


    @Override
    public List<LogEntry> search(SearchDto searchDto) throws Exception {
        SearchTemplateRequest request = new SearchTemplateRequest();
        request.setRequest(new SearchRequest(INDEX));
        request.setScriptType(ScriptType.INLINE);
        Map<String, Object> scriptParams = new HashMap<>();


        if (searchDto.getMessage()== null || searchDto.getMessage().equals("")) {
            request.setScript(noMessageTemplate);
        } else {

            String incomingSearchRequest = searchDto.getMessage();


            if (incomingSearchRequest.contains("=")) {
                String[] requestParameters = incomingSearchRequest.split("=");
                String keyword = checkIsDetails(requestParameters[0].trim()) + ".keyword";


                String message = requestParameters[1].trim();

                request.setScript(keyExactTemplate);
                scriptParams.put("field", keyword  );
                scriptParams.put("message", message);
            } else if (incomingSearchRequest.contains(":")) {
                String[] requestParameters = incomingSearchRequest.split(":");
                String keyword = checkIsDetails(requestParameters[0].trim());
                String message = requestParameters[1].trim();

                scriptParams.put("field", keyword);
                scriptParams.put("message", message);

                if (message.startsWith("\"") && message.endsWith("\"")) {
                    request.setScript(keyPhraseTemplate);
                } else {
                    request.setScript(keyAnyTemplate);
                }
            } else {
                scriptParams.put("message", incomingSearchRequest);
                if (incomingSearchRequest.startsWith("\"") && incomingSearchRequest.endsWith("\"")) {
                    request.setScript(phraseTemplate);
                } else {
                    request.setScript(anyTemplate);
                }
            }
        }

        scriptParams.put("timestamp", "timestamp");
        scriptParams.put("start", searchDto.getStartDate());
        scriptParams.put("end", searchDto.getEndDate());

        request.setScriptParams(scriptParams);

        SearchTemplateResponse searchTemplateResponse = client.
                searchTemplate(request, RequestOptions.DEFAULT);

        SearchResponse searchResponse = searchTemplateResponse.getResponse();


        return getSearchResult(searchResponse);

    }


    private String checkIsDetails(String keyword) {

        String[] values = {"username","ip","id","timestamp"};
        boolean contains = Arrays.stream(values).anyMatch(keyword::equals);

        if (contains) {
            return keyword;
        } else {
            return "details." + keyword;
        }
    }

}
